package jp.alhinc.kitamura_ryunosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {

		HashMap<String, String> codename = new HashMap<String, String>();
		HashMap<String, Long> codekingaku = new HashMap<String, Long>();
		ArrayList<Integer>number = new ArrayList<Integer>();
		
		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			if(file.exists()){
				//System.out.println();
			}else {
				System.out.println("支店定義ファイルが存在しません");
				return;
				}
			
			String line;//lineコードと支店名
			while ((line = br.readLine()) != null) {
				String[] data = line.split(",");
				if(data[0].matches("^\\d{3}")&& data.length==2) {
				}else{
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				
				codename.put(data[0], data[1]);
				codekingaku.put(data[0], (long) 0);
			}
			
		}catch (IOException e) {
			System.out.println("エラーが発生しました。");
		}

		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
			try {
				File files = new File(args[0]);
				String[] dr = files.list(); // dr//get name
											 
				for (String file : dr) {
					// String [] data = line.split("\\.");
					// System.out.println(file);
					// data0は数字8桁 data1はrcd
					if (file.matches("^\\d{8}.rcd$")) {
						
						String digit = file.substring(1, 8);
						int digit1 = Integer.parseInt(digit);
						number.add(digit1);
						
						try {
							File rcdfile = new File(args[0], file);
							FileReader fr = new FileReader(rcdfile);
							br = new BufferedReader(fr);//file各支店ファイル
							
							String shiten = br.readLine();	//支店名
							String kingaku = br.readLine();	//売上金額
							String gyou = br.readLine();	//3行目

							if(codename.get(shiten)==null){
								System.out.println(file+"の支店コードが不正です");
							}

							if(gyou!=null){
								System.out.println(file+"フォーマットが不正です");	
							}
							
							Long goukei = (long) Long.parseLong(kingaku);
							codekingaku.put(shiten,goukei+codekingaku.get(shiten));
							
							if(kingaku.matches("^\\d{1,10}$")){
							//System.out.println();
							}else{
								System.out.println("合計金額が10桁を超えました");
								return;
							}					
						} catch (IOException e) {
							System.out.println("予期せぬエラーが発生しました。");
						}
					}
				}

				Collections.sort(number);
				if (number.get(number.size() - 1) - number.get(0) != number.size() - 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
				BufferedWriter bw = null;
				try{
					File file1 = new File(args[0],"branch.out");
					FileWriter fw =new FileWriter(file1);
					 bw = new BufferedWriter(fw);
				
				for(Entry<String, Long> entry : codekingaku.entrySet()){
					String key=entry.getKey();
					Long value =entry.getValue();
					codekingaku.put(entry.getKey(), entry.getValue());
					String ef=(key+"," +codename.get(key) + "," + value);
					
					bw.write(ef+"\r\n");
				
				}
					bw.close();
				
				} catch (IOException e) {
					System.out.println(e);
				}
			} 
			 finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("closeできませんでした。");
					}
				}
			}
		}
	}
}
